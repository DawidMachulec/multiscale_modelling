﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace GrainGrowth
{
    public partial class Form1 : Form
    {
        struct element
        {
            public int value;
            public bool ifBlock;
            public bool ifBorder;
        }

        System.Random rand = new Random(0);

        // graph variable
        int graphHeight = 304;
        int graphWidth = 304;
        Graphics graph;

        //value table 
        element[,] table;
        element[,] tableOld;
        int []sum;

        // seed variables
        int x, y;
        int seedsNumber = 0;
        int propability = 0;

        //inclusion and boundaries variables
        int inclusionNumber = 0;
        int inclusionSize = 0;
        int inclusionType = 0;
        int boundariesSize = 0;

        // component initialization
        public Form1()
        {
            InitializeComponent();
        }

        // other function
        private void randSeed(int graphWidth, int graphHeight, ref int x, ref int y)
        {
            int i = 0;
           
            do
            {
                x = rand.Next(1, graphWidth - 4);
                y = rand.Next(1, graphHeight - 4);
                i++;
            } while (table[y, x].value != 0 && i < 10000);
        }

        private void randSeedOnBorder(int graphWidth, int graphHeight, ref int x, ref int y)
        {
            int i = 0;
            do
            {
                x = rand.Next(1, graphWidth - 3);
                y = rand.Next(1, graphHeight - 3);
                i++;
            } while (table[y + inclusionSize/2, x + inclusionSize / 2].value == table[y + inclusionSize / 2, x + inclusionSize / 2 + 1].value &&
                    table[y + inclusionSize / 2, x + inclusionSize / 2].value == table[y + inclusionSize / 2 + 1, x + inclusionSize / 2].value &&
                    table[y + inclusionSize / 2, x + inclusionSize / 2].value == table[y + inclusionSize / 2 + 1, x + inclusionSize / 2 + 1].value &&
                    i < 10000);
        }

        private void drawPoint(int i, int x, int y)
        {
            switch (i)
            {
                case 0:
                    graph.DrawRectangle(Pens.White, x, y, 1, 1);
                    break;
                case 1:
                    graph.DrawRectangle(Pens.Green, x, y, 1, 1);
                    break;
                case 2:
                    graph.DrawRectangle(Pens.Blue, x, y, 1, 1);
                    break;
                case 3:
                    graph.DrawRectangle(Pens.Violet, x, y, 1, 1);
                    break;
                case 4:
                    graph.DrawRectangle(Pens.Red, x, y, 1, 1);
                    break;
                case 5:
                    graph.DrawRectangle(Pens.Orange, x, y, 1, 1);
                    break;
                case 6:
                    graph.DrawRectangle(Pens.Black, x, y, 1, 1);
                    break;
                case 7:
                    graph.DrawRectangle(Pens.Yellow, x, y, 1, 1);
                    break;
            }
        }

        private void simpleGrowth()
        {
            int index = 0;
            rewriteTable();
            for (int i = 1; i < (graphHeight - 3); i++)
            {
                for (int j = 1; j < (graphWidth - 3); j++)
                {
                    if (tableOld[i, j].value == 0)
                    {
                        clearSum();
                        checkNeighbour(i, j);
                        index = chooseMax();
                        if (index != 0)
                        {
                            //drawPoint(index, j, i);
                            table[i, j].value = index;
                            table[i, j].ifBlock = false;
                        }
                    }
                }
            }
            //rewriteTable();
        }

        private void grainCurvature()
        {
            int index = 0;
            rewriteTable();
            for (int i = 1; i < (graphHeight - 3); i++)
            {
                for (int j = 1; j < (graphWidth - 3); j++)
                {
                    if (tableOld[i, j].value == 0 && tableOld[i, j].ifBlock == false)
                    {
                        // 1 rule
                        clearSum();
                        checkNeighbour(i, j); 
                        index = firstRule();

                        if (index == 0)
                        {
                            // 2 rule
                            clearSum();
                            checkNearestNegihbour(i, j);
                            index = secoundRule(); 

                            if (index == 0)
                            {
                                // 3 rule
                                clearSum();
                                checkFurtherNegihbour(i, j);
                                index = thirdRule();

                                if (index == 0)
                                {
                                    // 4 rule
                                    if (rand.Next(1, 100) < propability)
                                    {
                                        clearSum();
                                        checkNeighbour(i, j);
                                        index = chooseMax();
                                    }
                                    else
                                        continue;
                                }
                            }
                        }

                        if (index != 0)
                        {
                            //drawPoint(index, j, i);
                            table[i, j].value = index;
                            table[i, j].ifBlock = false;
                        }
                    }
                }
            }
            //rewriteTable();
        }

        private int firstRule()
        {
            int min = 5;
            int index = 0;

            for (int i = 1; i < 6; i++)
            {
              
                if (sum[i] >= min)
                {
                    index = i;
                    //break;
                }
            }

            return index;
        }

        private int secoundRule()
        {
            int min = 3;
            int index = 0;

            for (int i = 1; i < 6; i++)
            {

                if (sum[i] >= min)
                {
                    index = i;
                    //break;
                }
            }

            return index;
        }

        private int thirdRule()
        {
            int min = 3;
            int index = 0;

            for (int i = 1; i < 6; i++)
            {

                if (sum[i] >= min)
                {
                    index = i;
                    //break;
                }
            }

            return index;
        }

        private void clearTable()
        {
            for (int i = 1; i < (graphHeight - 1); i++)
            {
                for (int j = 1; j < (graphWidth - 1); j++)
                {
                    if (table[i, j].ifBlock == false && table[i, j].value != 0)
                    {
                        table[i, j].value = 0;
                        tableOld[i, j].value = 0;
                        //drawPoint(0, j, i);
                    }
                }
            }
            drawAll();
        }

        private void checkNeighbour(int i, int j)
        {
            if (tableOld[i - 1, j - 1].ifBlock == false)
                sum[tableOld[i - 1, j - 1].value]++;
            if (tableOld[i - 1, j].ifBlock == false)
                sum[tableOld[i - 1, j].value]++;
            if (tableOld[i - 1, j + 1].ifBlock == false)
                sum[tableOld[i - 1, j + 1].value]++;
            if (tableOld[i, j - 1].ifBlock == false)
                sum[tableOld[i, j - 1].value]++;
            if (tableOld[i, j + 1].ifBlock == false)
                sum[tableOld[i, j + 1].value]++;
            if (tableOld[i + 1, j - 1].ifBlock == false)
                sum[tableOld[i + 1, j - 1].value]++;
            if (tableOld[i + 1, j].ifBlock == false)
                sum[tableOld[i + 1, j].value]++;
            if (tableOld[i + 1, j + 1].ifBlock == false)
                sum[tableOld[i + 1, j + 1].value]++;
        }

        private void checkNearestNegihbour(int i, int j)
        {
            if (tableOld[i - 1, j].ifBlock == false)
                sum[tableOld[i - 1, j].value]++;
            if (tableOld[i, j - 1].ifBlock == false)
                sum[tableOld[i, j - 1].value]++;
            if (tableOld[i, j + 1].ifBlock == false)
                sum[tableOld[i, j + 1].value]++;
            if (tableOld[i + 1, j].ifBlock == false)
                sum[tableOld[i + 1, j].value]++;
        }

        private void checkFurtherNegihbour(int i, int j)
        {
            if (tableOld[i - 1, j - 1].ifBlock == false)
                sum[tableOld[i - 1, j - 1].value]++;
            if (tableOld[i - 1, j + 1].ifBlock == false)
                sum[tableOld[i - 1, j + 1].value]++;
            if (tableOld[i + 1, j - 1].ifBlock == false)
                sum[tableOld[i + 1, j - 1].value]++;
            if (tableOld[i + 1, j + 1].ifBlock == false)
                sum[tableOld[i + 1, j + 1].value]++;
        }

        private int chooseMax()
        {
            int max = 0;
            int index = 0;
            bool flag = false;
            for (int i = 1; i < 6; i++)
            {
                if (sum[i] == max)
                {
                    flag = true;
                }
                else if (sum[i] > max)
                {
                    index = i;
                    max = sum[i];
                    flag = false;
                }
            }

            //if (flag == true)
            //    index = 0;

            return index;
        }

        private void clearSum()
        {
            for (int i = 0; i < 7; i++)
            {
                sum[i] = 0;
            }
        }

        private void rewriteTable()
        {
            for (int i = 0; i < (graphHeight-2); i++)
            {
                for (int j = 0; j < (graphWidth-2); j++)
                {
                    tableOld[i, j].value = table[i, j].value;
                    tableOld[i, j].ifBlock = table[i, j].ifBlock;
                    tableOld[i, j].ifBorder = table[i, j].ifBorder;
                }
            }
        }

        /*
        private void drawAll()
        {
            for (int i = 0; i < (graphHeight - 4); i++)
            {
                for (int j = 0; j < (graphWidth - 4); j++)
                {
                    if (table[i, j].value != 0 && table[i,j].value != 9)
                    {
                        drawPoint(table[i, j].value, j, i);
                    }
                }
            }
        }
        */

        private void drawAll()
        {
            for (int i = 0; i < (graphHeight - 2); i++)
            {
                for (int j = 0; j < (graphWidth - 2); j++)
                {
                    drawPoint(table[i, j].value, j, i);
                }
            }
        }

        private bool checkTable()
        {
            bool flag = true;

            for (int i = 1; i < (graphHeight - 3); i++)
            {
                for (int j = 1; j < (graphWidth - 3); j++)
                {
                    if (tableOld[i, j].value == 0)
                    {
                        return flag = false;
                    }
                }
            }

            return flag;
        }

        private void startSettings()
        {
            panel1.Size = new System.Drawing.Size(graphWidth, graphHeight);
            graph = panel1.CreateGraphics();
            table = new element[graphHeight, graphWidth];
            tableOld = new element[graphHeight, graphWidth];
            sum = new int[7];
            graph.Clear(Color.White);
            //initialTableZero();
        }

        private void initialTableZero()
        {
            for (int i = 0; i < (graphHeight - 2); i++)
            {
                for (int j = 0; j < (graphWidth - 2); j++)
                {
                    table[i, j].value = 0;
                    tableOld[i, j].value = 0;
                }
            }
        }

        private void addSquareInclusion(int x, int y)
        {
            for (int i = 0; i < inclusionSize; i++)
            {
                for (int j = 0; j < inclusionSize; j++)
                {
                    drawPoint(6, x + j, y + i);
                    table[y + i, x + j].value = 6;
                    table[y + i, x + j].ifBlock = true;
                }
            }
        }

        private void addCircleInclusion(int x, int y)
        {
            double dist = 0.0;
            double middle = (inclusionSize / 2.0);

            for (int i = 0; i < inclusionSize; i++)
            {
                for (int j = 0; j < inclusionSize; j++)
                {
                    dist = Math.Sqrt(Math.Pow((middle - i), 2) + (Math.Pow((middle - j), 2)));
                    if (dist <= middle)
                    {
                        drawPoint(6, x + j, y + i);
                        table[y + i, x + j].value = 6;
                        table[y + i, x + j].ifBlock = true;
                    }
                }
            }
        }

        private void clearInclusion()
        {
            for (int i = 1; i < (graphHeight - 3); i++)
            {
                for (int j = 1; j < (graphWidth - 3); j++)
                {
                    if (table[i, j].ifBlock == true)
                    {
                        table[i, j].value = 0;
                        table[i, j].ifBlock = false;
                        drawPoint(0, j, i);
                    }
                }
            }
        }

        private void findBoundaries()
        {
            for (int i = 1; i < (graphHeight - 4); i++)
            {
                for (int j = 1; j < (graphWidth - 4); j++)
                {
                    if ((table[i, j].value != table[i + 1, j + 1].value) && (table[i, j].value != 6 && table[i + 1, j + 1].value != 6))
                    {
                        table[i, j].ifBorder = true;
                        table[i, j].ifBlock = true;

                        table[i + 1, j + 1].ifBorder = true;
                        table[i + 1, j + 1].ifBlock = true;
                    }

                    if ((table[i, j].value != table[i + 1, j].value) && (table[i, j].value != 6 && table[i + 1, j].value != 6))
                    {
                        table[i, j].ifBorder = true;
                        table[i, j].ifBlock = true;

                        table[i + 1, j].ifBorder = true;
                        table[i + 1, j].ifBlock = true;
                    }

                    if ((table[i, j].value != table[i, j + 1].value) && (table[i, j].value != 6 && table[i, j + 1].value != 6))
                    {
                        table[i, j].ifBorder = true;
                        table[i, j].ifBlock = true;

                        table[i, j + 1].ifBorder = true;
                        table[i, j + 1].ifBlock = true;
                    }
                }
            }
        }

        private void findBoundariesSelected()
        {
            for (int k = 0; k < ChoosenElement.Items.Count; k++)
            {
                int elementIndex = toIndex(Convert.ToString(ChoosenElement.Items[k]));

                for (int i = 2; i < (graphHeight - 4); i++)
                {
                    for (int j = 2; j < (graphWidth - 4); j++)
                    {
                        if (table[i,j].value == elementIndex)
                        {
                            if (table[i-1,j-1].value != elementIndex && table[i - 1, j - 1].value != 6)
                            {
                                table[i - 1, j - 1].ifBorder = true;
                                table[i - 1, j - 1].ifBlock = true;

                                table[i, j].ifBorder = true;
                                table[i, j].ifBlock = true;
                            }

                            if (table[i, j - 1].value != elementIndex && table[i, j - 1].value != 6)
                            {
                                table[i, j - 1].ifBorder = true;
                                table[i, j - 1].ifBlock = true;

                                table[i, j].ifBorder = true;
                                table[i, j].ifBlock = true;
                            }

                            if (table[i - 1, j].value != elementIndex && table[i - 1, j].value != 6)
                            {
                                table[i - 1, j].ifBorder = true;
                                table[i - 1, j].ifBlock = true;

                                table[i, j].ifBorder = true;
                                table[i, j].ifBlock = true;
                            }

                            if (table[i - 1, j + 1].value != elementIndex && table[i - 1, j + 1].value != 6)
                            {
                                table[i - 1, j + 1].ifBorder = true;
                                table[i - 1, j + 1].ifBlock = true;

                                table[i, j].ifBorder = true;
                                table[i, j].ifBlock = true;
                            }

                            if (table[i + 1, j - 1].value != elementIndex && table[i + 1, j - 1].value != 6)
                            {
                                table[i + 1, j - 1].ifBorder = true;
                                table[i + 1, j - 1].ifBlock = true;

                                table[i, j].ifBorder = true;
                                table[i, j].ifBlock = true;
                            }

                            if (table[i + 1, j].value != elementIndex && table[i + 1, j].value != 6)
                            {
                                table[i + 1, j].ifBorder = true;
                                table[i + 1, j].ifBlock = true;

                                table[i, j].ifBorder = true;
                                table[i, j].ifBlock = true;
                            }

                            if (table[i, j + 1].value != elementIndex && table[i, j + 1].value != 6)
                            {
                                table[i, j + 1].ifBorder = true;
                                table[i, j + 1].ifBlock = true;

                                table[i, j].ifBorder = true;
                                table[i, j].ifBlock = true;
                            }

                            if (table[i + 1, j + 1].value != elementIndex && table[i + 1, j + 1].value != 6)
                            {
                                table[i + 1, j + 1].ifBorder = true;
                                table[i + 1, j + 1].ifBlock = true;

                                table[i, j].ifBorder = true;
                                table[i, j].ifBlock = true;
                            }
                        }
                    }
                }
            }
        }

        private void colorBoundaries()
        {
            for (int i = 1 + boundariesSize; i < (graphHeight - 2 - boundariesSize); i++)
            {
                for (int j = 1 + boundariesSize; j < (graphWidth - 2 - boundariesSize); j++)
                {
                    if (table[i, j].ifBorder == true)
                    {
                        for (int k = 0; k < boundariesSize*2; k++)
                        {
                            //drawPoint(6, j-boundariesSize+k, i-boundariesSize+k);
                            table[i - boundariesSize + k, j - boundariesSize + k].value = 6;
                            table[i - boundariesSize + k, j - boundariesSize + k].ifBlock = true;

                            table[i, j - boundariesSize + k].value = 6;
                            table[i, j - boundariesSize + k].ifBlock = true;

                            table[i - boundariesSize + k,j].value = 6;
                            table[i - boundariesSize + k,j].ifBlock = true;

                            table[i + boundariesSize - k, j - boundariesSize + k].value = 6;
                            table[i + boundariesSize - k, j - boundariesSize + k].ifBlock = true;
                        }
                    }
                }
            }
            drawAll();
        }

        private void clearBoundaries()
        {
            for (int i = 1; i < (graphHeight - 3); i++)
            {
                for (int j = 1; j < (graphWidth - 3); j++)
                {
                    table[i, j].ifBorder = false;
                }
            }
        }

        private void addElementsToChoose()
        {
            string elementColor = "";

            elementToChoose.Items.Clear();
            for (int i = 1; i <= seedsNumber+1; i++)
            {
                switch (i)
                {
                    case 1:
                        elementColor = "Green";
                        break;
                    case 2:
                        elementColor = "Blue";
                        break;
                    case 3:
                        elementColor = "Violet";
                        break;
                    case 4:
                        elementColor = "Red";
                        break;
                    case 5:
                        elementColor = "Orange";
                        break;
                }

                elementToChoose.Items.Add(elementColor);
            }

        }

        private void blockElement(int elementIndex)
        {
            for (int i = 1; i < (graphHeight - 3); i++)
            {
                for (int j = 1; j < (graphWidth - 3); j++)
                {
                    if (table[i, j].value == elementIndex)
                        table[i, j].ifBlock = true;
                }
            }
        }

        private void changeColor(int elementIndex)
        {
            for (int i = 1; i < (graphHeight - 3); i++)
            {
                for (int j = 1; j < (graphWidth - 3); j++)
                {
                    if (table[i, j].value == elementIndex)
                    {
                        table[i, j].value = 7;
                        graph.DrawRectangle(Pens.Yellow, j, i, 1, 1);
                    }
                }
            }
        }

        private int toIndex(string item)
        {
            int elementIndex = 0;

            switch (item)
            {
                case "Green":
                    elementIndex = 1;
                    break;
                case "Blue":
                    elementIndex = 2;
                    break;
                case "Violet":
                    elementIndex = 3;
                    break;
                case "Red":
                    elementIndex = 4;
                    break;
                case "Orange":
                    elementIndex = 5;
                    break;
            }

            return elementIndex;
        }

        private double countBloced()
        {
            double count = 0.0;
            double countAll = 0.0;
            double percentage = 0.0;
            for (int i = 1; i < (graphHeight - 3); i++)
            {
                for (int j = 1; j < (graphWidth - 3); j++)
                {
                    if (table[i, j].value == 6)
                        count++;

                    countAll++;
                }
            }
            percentage = count / countAll * 100;
            percentage = Math.Round(percentage, 2);
            return percentage;
        }

        // panel controls
        private void Form1_Load(object sender, EventArgs e)
        {
            startSettings();
        }

        private void numberOfSeeds_SelectedIndexChanged(object sender, EventArgs e)
        {
            seedsNumber = numberOfSeeds.SelectedIndex;
        }

        private void simpleGrowth_Click(object sender, EventArgs e)
        {
            bool flag = false;
            int i = 0;
            while (i < 10000 && flag == false)
            {
                simpleGrowth();
                i++;
                flag = checkTable();
            }
            drawAll();
           
            //graph.DrawRectangle(Pens.Green, 1, 1, 1, 1);
            //graph.DrawRectangle(Pens.Green, graphHeight-5, graphWidth-5, 1, 1);
        }

        private void setGraph_Click(object sender, EventArgs e)
        {
            int tempGraphHeight = 100;
            int tempGraphWifth = 100;
            try
            {
                tempGraphHeight = Convert.ToInt32(LabelHight.Text);
                tempGraphWifth = Convert.ToInt32(LabelWidth.Text);

                if (tempGraphHeight > 99 && tempGraphHeight < 401 && tempGraphWifth > 99 && tempGraphWifth < 401)
                {
                    graphHeight = tempGraphHeight;
                    graphWidth = tempGraphWifth;

                    startSettings();
                }
                else
                    MessageBox.Show("You enter wrong value, you could use namber from 100 to 400", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch
            {
                MessageBox.Show("You have to type some value", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }


        }

        
        private void export_Click(object sender, EventArgs e)
        {
            if (exportFileDialog.ShowDialog() == DialogResult.OK)
            {
                //File.WriteAllText(exportFileDialog.FileName, "My first tekst");
                StreamWriter sw = new StreamWriter(exportFileDialog.FileName);
                
                switch (exportFileDialog.FilterIndex)
                {
                    case 1:
                        sw.Write(Convert.ToString(graphHeight));
                        sw.Write("\n");
                        sw.Write(Convert.ToString(graphHeight));
                        sw.Write("\n");

                        for (int i = 0; i < (graphHeight - 1); i++)
                        {
                            for (int j = 0; j < (graphHeight - 1); j++)
                            {
                                sw.Write(Convert.ToString(table[i, j].value));
                                //sw.Write(";");
                            }
                            //sw.Write("\n");
                        }
                        
                        break;
                    case 2:

                        break;
                    case 3:

                        break;

                }
            }
        }

                       
        private void import_Click(object sender, EventArgs e)
        {
            if (importFileDialog.ShowDialog() == DialogResult.OK)
            {
                //File.WriteAllText(exportFileDialog.FileName, "My first tekst");
                StreamReader sw = new StreamReader(importFileDialog.FileName);
                graphHeight = Convert.ToInt32(sw.ReadLine());
                graphWidth = Convert.ToInt32(sw.ReadLine());

                if (graphHeight > 99 && graphHeight < 401 && graphWidth > 99 && graphWidth < 401)
                {
                    LabelHight.Text = Convert.ToString(graphHeight);
                    LabelWidth.Text = Convert.ToString(graphWidth);

                    startSettings();
                }
                else
                    MessageBox.Show("Size is wrong", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                for (int i = 0; i < (graphHeight - 1); i++)
                {
                    for (int j = 0; j < (graphWidth - 1); j++)
                    {
                        table[i, j].value = sw.Read() - 48;
                        //sw.Read();
                        //table[i, j].value = sw.Peek() - 48;
                    }
                }
            }
            drawAll();
        }

        private void numberOfinclusion_SelectedIndexChanged(object sender, EventArgs e)
        {
            inclusionNumber = numberOfinclusion.SelectedIndex;
        }

        private void inclusionRandomButton_Click(object sender, EventArgs e)
        {
            try
            {
                inclusionSize = Convert.ToInt32(sizeOfIlncusion.Text) * 2;
            }
            catch
            {
                MessageBox.Show("You have to enter size of inclusion", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            if (inclusionSize > 0)
            {
                //clearInclusion();
                bool flag = checkTable();

                for (int i = 1; i <= inclusionNumber + 1; i++)
                {
                    if (!flag)
                    {
                        randSeed(graphHeight - (inclusionSize + 1), graphWidth - (inclusionSize + 1), ref x, ref y);
                    }
                    else
                    {
                        randSeedOnBorder(graphHeight - (inclusionSize + 1), graphWidth - (inclusionSize + 1), ref x, ref y);
                    }

                    if (inclusionType == 0)
                        addSquareInclusion(x, y);
                    else if (inclusionType == 1)
                        addCircleInclusion(x, y);

                }
            }
            else
                MessageBox.Show("Size of inclusion have to be more then 0", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void typeOfInclusion_SelectedIndexChanged(object sender, EventArgs e)
        {
            inclusionType = typeOfInclusion.SelectedIndex;
        }

        private void grainCurvatureButton(object sender, EventArgs e)
        {
            bool flag = false;
            int i = 0;

            try
            {
                propability = Convert.ToInt32(textPropability.Text);
            }
            catch
            {
                MessageBox.Show("You have to type some value in Propability", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }


            if (propability > 0)
            {
                while (i < 10000 && flag == false)
                {
                    grainCurvature();
                    i++;
                    flag = checkTable();
                }
                drawAll();
            }
            else
                MessageBox.Show("Propability have to be more then 0", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void allBound_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(boundSize.Text) > 0)
                    boundariesSize = Convert.ToInt32(boundSize.Text);
                else
                    MessageBox.Show("Boundaries size have to be more then 0", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                clearBoundaries();
                findBoundaries();
                colorBoundaries();
            }
            catch
            {
                MessageBox.Show("You have to enter boundaries size", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void rmMatterial_Click(object sender, EventArgs e)
        {
            clearTable();
            //colorBoundaries();
        }

        private void rmAll_Click(object sender, EventArgs e)
        {
            clearTable();
            for (int i = 1; i < (graphHeight - 1); i++)
            {
                for (int j = 1; j < (graphWidth - 1); j++)
                {
                    table[i, j].ifBorder = false;
                    table[i, j].ifBlock = false;
                    table[i, j].value = 0;
                }
            }
            graph.Clear(Color.White);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ChoosenElement.Items.Contains(elementToChoose.SelectedItem))
                    ChoosenElement.Items.Add(elementToChoose.SelectedItem);
                else
                    MessageBox.Show("You coud not choose the element twice", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch
            {
                MessageBox.Show("You have to chose an element to add", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
                if (ChoosenElement.SelectedIndex != -1)
                    ChoosenElement.Items.RemoveAt(ChoosenElement.SelectedIndex);
                else
                    MessageBox.Show("You have to chose an element to remove", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void Dp_Click(object sender, EventArgs e)
        {
            int elementIndex = 0;

            if (ChoosenElement.Items.Count > 0)
            {
                for (int i = 0; i < ChoosenElement.Items.Count; i++)
                {
                    elementIndex = toIndex(Convert.ToString(ChoosenElement.Items[i]));

                    /*
                    switch (ChoosenElement.Items[i])
                    {
                        case "Green":
                            elementIndex = 1;
                            break;
                        case "Blue":
                            elementIndex = 2;
                            break;
                        case "Violet":
                            elementIndex = 3;
                            break;
                        case "Red":
                            elementIndex = 4;
                            break;
                        case "Orange":
                            elementIndex = 5;
                            break;
                    }
                    */

                    blockElement(elementIndex);
                    changeColor(elementIndex);
                }
            }
            else
                MessageBox.Show("You have to chose an element", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void Sub_Click(object sender, EventArgs e)
        {
            int elementIndex = 0;

            if (ChoosenElement.Items.Count > 0)
            {
                for (int i = 0; i < ChoosenElement.Items.Count; i++)
                {
                    elementIndex = toIndex(Convert.ToString(ChoosenElement.Items[i]));
                    /*
                    switch (ChoosenElement.Items[i])
                    {
                        case "Green":
                            elementIndex = 1;
                            break;
                        case "Blue":
                            elementIndex = 2;
                            break;
                        case "Violet":
                            elementIndex = 3;
                            break;
                        case "Red":
                            elementIndex = 4;
                            break;
                        case "Orange":
                            elementIndex = 5;
                            break;
                    }
                    */
                    blockElement(elementIndex);
                }
            }
            else
                MessageBox.Show("You have to chose an element", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void selectedBorder_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(boundSize.Text) > 0)
                    boundariesSize = Convert.ToInt32(boundSize.Text);
                else
                    MessageBox.Show("Boundaries size have to be more then 0", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                clearBoundaries();
                findBoundariesSelected();
                colorBoundaries();
            }
            catch
            {
                MessageBox.Show("You have to enter boundaries size", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void percentageOfBoundariesButton_Click(object sender, EventArgs e)
        {
            double percentage = 0.0;

            percentage = countBloced();

            percentageOfBoundaries.Text = Convert.ToString(percentage);
        }

        private void randomSeeds_Click(object sender, EventArgs e)
        {
            clearTable();
            //graph.Clear(Color.White);
            for (int i = 1; i <= seedsNumber+1; i++)
            {
                randSeed(graphHeight, graphWidth, ref x, ref y);
                drawPoint(i, x, y);
                table[y, x].value = i;
                table[y, x].ifBlock = false;
            }

            addElementsToChoose();
        }
    }
}
