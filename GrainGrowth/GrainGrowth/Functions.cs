﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Functions
{
    public class functions
    {
        System.Random rand = new Random(0);

        // other function
        public void randSeed(int graphWidth, int graphHeight, ref int x, ref int y)
        {
            x = rand.Next(1, graphWidth - 5);
            y = rand.Next(1, graphHeight - 5);
        }

        public void drawPoint(int i, int x, int y)
        {
            switch (i)
            {
                case 1:
                    graph.DrawRectangle(Pens.Green, x, y, 1, 1);
                    break;
                case 2:
                    graph.DrawRectangle(Pens.Blue, x, y, 1, 1);
                    break;
                case 3:
                    graph.DrawRectangle(Pens.Violet, x, y, 1, 1);
                    break;
                case 4:
                    graph.DrawRectangle(Pens.Red, x, y, 1, 1);
                    break;
                case 5:
                    graph.DrawRectangle(Pens.Orange, x, y, 1, 1);
                    break;
                case 6:
                    graph.DrawRectangle(Pens.Black, x, y, 1, 1);
                    break;
            }
        }

        public void simpleGrowth()
        {
            int index = 0;
            for (int i = 1; i < (graphHeight - 5); i++)
            {
                for (int j = 1; j < (graphWidth - 5); j++)
                {
                    if (tableOld[i, j] == 0)
                    {
                        clearSum();
                        checkNeighbour(i, j);
                        index = chooseMax();
                        if (index != 0)
                        {
                            //drawPoint(index, j, i);
                            table[i, j] = index;
                        }
                    }
                }
            }
            rewriteTable();
        }

        public void clearTable()
        {
            for (int i = 1; i < (graphHeight - 5); i++)
            {
                for (int j = 1; j < (graphWidth - 5); j++)
                {
                    table[i, j] = 0;
                    tableOld[i, j] = 0;
                }
            }
        }

        public void checkNeighbour(int i, int j)
        {
            sum[tableOld[i - 1, j - 1]]++;
            sum[tableOld[i - 1, j]]++;
            sum[tableOld[i - 1, j + 1]]++;
            sum[tableOld[i, j - 1]]++;
            sum[tableOld[i, j + 1]]++;
            sum[tableOld[i + 1, j - 1]]++;
            sum[tableOld[i + 1, j]]++;
            sum[tableOld[i + 1, j + 1]]++;
        }

        public int chooseMax()
        {
            int max = 0;
            int index = 0;
            bool flag = false;
            for (int i = 1; i < 6; i++)
            {
                if (sum[i] == max)
                {
                    flag = true;
                }
                else if (sum[i] > max)
                {
                    index = i;
                    max = sum[i];
                    flag = false;
                }
            }

            if (flag == true)
                index = 0;

            return index;
        }

        public void clearSum()
        {
            for (int i = 0; i < 7; i++)
            {
                sum[i] = 0;
            }
        }

        public void rewriteTable()
        {
            for (int i = 0; i < (graphHeight - 4); i++)
            {
                for (int j = 0; j < (graphWidth - 4); j++)
                {
                    tableOld[i, j] = table[i, j];
                }
            }
        }

        public void drawAll()
        {
            for (int i = 0; i < (graphHeight - 4); i++)
            {
                for (int j = 0; j < (graphWidth - 4); j++)
                {
                    if (table[i, j] != 0 && table[i, j] != 9)
                    {
                        drawPoint(table[i, j], j, i);
                    }
                }
            }
        }

        public bool checkTable()
        {
            bool flag = true;
            for (int i = 1; i < (graphHeight - 5); i++)
            {
                for (int j = 1; j < (graphWidth - 5); j++)
                {
                    if (tableOld[i, j] == 0)
                    {
                        return flag = false;
                    }
                }
            }
            return flag;
        }

        public void startSettings()
        {
            panel1.Size = new System.Drawing.Size(graphWidth, graphHeight);
            graph = panel1.CreateGraphics();
            graph.Clear(Color.White);
            table = new int[graphHeight - 4, graphWidth - 4];
            tableOld = new int[graphHeight - 4, graphWidth - 4];
            sum = new int[7];
        }

        public void drawInclusion(int x, int y)
        {
            graph.DrawRectangle(Pens.Black, x - 1, y - 1, 3, 3);
        }
    }
}