﻿namespace GrainGrowth
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.randomSeeds = new System.Windows.Forms.Button();
            this.numberOfSeeds = new System.Windows.Forms.ComboBox();
            this.simpleGrowthButton = new System.Windows.Forms.Button();
            this.groupBoxSimpleGrainGrowth = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textPropability = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.grianCurvatureButton = new System.Windows.Forms.Button();
            this.groupBoxGraph = new System.Windows.Forms.GroupBox();
            this.setGraph = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LabelWidth = new System.Windows.Forms.TextBox();
            this.LabelHight = new System.Windows.Forms.TextBox();
            this.importFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.exportFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.export = new System.Windows.Forms.Button();
            this.import = new System.Windows.Forms.Button();
            this.inclusion = new System.Windows.Forms.GroupBox();
            this.sizeOfIlncusion = new System.Windows.Forms.TextBox();
            this.typeOfInclusion = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.inclusionRandomButton = new System.Windows.Forms.Button();
            this.numberOfinclusion = new System.Windows.Forms.ComboBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.grainBoundaries = new System.Windows.Forms.GroupBox();
            this.selectedBorder = new System.Windows.Forms.Button();
            this.rmAll = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.rmMatterial = new System.Windows.Forms.Button();
            this.boundSize = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.allBound = new System.Windows.Forms.Button();
            this.elementToChoose = new System.Windows.Forms.ListBox();
            this.ChoosenElement = new System.Windows.Forms.ListBox();
            this.addButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Sub = new System.Windows.Forms.Button();
            this.Dp = new System.Windows.Forms.Button();
            this.percentageOfBoundaries = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.percentageOfBoundariesButton = new System.Windows.Forms.Button();
            this.groupBoxSimpleGrainGrowth.SuspendLayout();
            this.groupBoxGraph.SuspendLayout();
            this.inclusion.SuspendLayout();
            this.grainBoundaries.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(440, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 400);
            this.panel1.TabIndex = 0;
            // 
            // randomSeeds
            // 
            this.randomSeeds.Location = new System.Drawing.Point(57, 22);
            this.randomSeeds.Name = "randomSeeds";
            this.randomSeeds.Size = new System.Drawing.Size(127, 25);
            this.randomSeeds.TabIndex = 1;
            this.randomSeeds.TabStop = false;
            this.randomSeeds.Text = "random seeds";
            this.randomSeeds.UseVisualStyleBackColor = true;
            this.randomSeeds.Click += new System.EventHandler(this.randomSeeds_Click);
            // 
            // numberOfSeeds
            // 
            this.numberOfSeeds.FormattingEnabled = true;
            this.numberOfSeeds.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.numberOfSeeds.Location = new System.Drawing.Point(6, 21);
            this.numberOfSeeds.Name = "numberOfSeeds";
            this.numberOfSeeds.Size = new System.Drawing.Size(45, 24);
            this.numberOfSeeds.TabIndex = 2;
            this.numberOfSeeds.SelectedIndexChanged += new System.EventHandler(this.numberOfSeeds_SelectedIndexChanged);
            // 
            // simpleGrowthButton
            // 
            this.simpleGrowthButton.Location = new System.Drawing.Point(40, 56);
            this.simpleGrowthButton.Name = "simpleGrowthButton";
            this.simpleGrowthButton.Size = new System.Drawing.Size(118, 25);
            this.simpleGrowthButton.TabIndex = 3;
            this.simpleGrowthButton.Text = "simple growth";
            this.simpleGrowthButton.UseVisualStyleBackColor = true;
            this.simpleGrowthButton.Click += new System.EventHandler(this.simpleGrowth_Click);
            // 
            // groupBoxSimpleGrainGrowth
            // 
            this.groupBoxSimpleGrainGrowth.Controls.Add(this.label5);
            this.groupBoxSimpleGrainGrowth.Controls.Add(this.textPropability);
            this.groupBoxSimpleGrainGrowth.Controls.Add(this.label4);
            this.groupBoxSimpleGrainGrowth.Controls.Add(this.grianCurvatureButton);
            this.groupBoxSimpleGrainGrowth.Controls.Add(this.numberOfSeeds);
            this.groupBoxSimpleGrainGrowth.Controls.Add(this.simpleGrowthButton);
            this.groupBoxSimpleGrainGrowth.Controls.Add(this.randomSeeds);
            this.groupBoxSimpleGrainGrowth.Location = new System.Drawing.Point(28, 119);
            this.groupBoxSimpleGrainGrowth.Name = "groupBoxSimpleGrainGrowth";
            this.groupBoxSimpleGrainGrowth.Size = new System.Drawing.Size(200, 168);
            this.groupBoxSimpleGrainGrowth.TabIndex = 4;
            this.groupBoxSimpleGrainGrowth.TabStop = false;
            this.groupBoxSimpleGrainGrowth.Text = "grain growth";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(138, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "%";
            // 
            // textPropability
            // 
            this.textPropability.Location = new System.Drawing.Point(90, 97);
            this.textPropability.Name = "textPropability";
            this.textPropability.Size = new System.Drawing.Size(42, 22);
            this.textPropability.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Propability";
            // 
            // grianCurvatureButton
            // 
            this.grianCurvatureButton.Location = new System.Drawing.Point(40, 131);
            this.grianCurvatureButton.Name = "grianCurvatureButton";
            this.grianCurvatureButton.Size = new System.Drawing.Size(118, 25);
            this.grianCurvatureButton.TabIndex = 4;
            this.grianCurvatureButton.Text = "grian curvature";
            this.grianCurvatureButton.UseVisualStyleBackColor = true;
            this.grianCurvatureButton.Click += new System.EventHandler(this.grainCurvatureButton);
            // 
            // groupBoxGraph
            // 
            this.groupBoxGraph.Controls.Add(this.setGraph);
            this.groupBoxGraph.Controls.Add(this.label1);
            this.groupBoxGraph.Controls.Add(this.LabelWidth);
            this.groupBoxGraph.Controls.Add(this.LabelHight);
            this.groupBoxGraph.Location = new System.Drawing.Point(28, 13);
            this.groupBoxGraph.Name = "groupBoxGraph";
            this.groupBoxGraph.Size = new System.Drawing.Size(200, 100);
            this.groupBoxGraph.TabIndex = 5;
            this.groupBoxGraph.TabStop = false;
            this.groupBoxGraph.Text = "graph";
            // 
            // setGraph
            // 
            this.setGraph.Location = new System.Drawing.Point(57, 59);
            this.setGraph.Name = "setGraph";
            this.setGraph.Size = new System.Drawing.Size(75, 25);
            this.setGraph.TabIndex = 3;
            this.setGraph.Text = "set";
            this.setGraph.UseVisualStyleBackColor = true;
            this.setGraph.Click += new System.EventHandler(this.setGraph_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(90, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "x";
            // 
            // LabelWidth
            // 
            this.LabelWidth.Location = new System.Drawing.Point(110, 22);
            this.LabelWidth.Name = "LabelWidth";
            this.LabelWidth.Size = new System.Drawing.Size(66, 22);
            this.LabelWidth.TabIndex = 1;
            // 
            // LabelHight
            // 
            this.LabelHight.Location = new System.Drawing.Point(18, 22);
            this.LabelHight.Name = "LabelHight";
            this.LabelHight.Size = new System.Drawing.Size(66, 22);
            this.LabelHight.TabIndex = 0;
            // 
            // importFileDialog
            // 
            this.importFileDialog.DefaultExt = "txt";
            this.importFileDialog.InitialDirectory = "C:\\";
            this.importFileDialog.Title = "import";
            // 
            // exportFileDialog
            // 
            this.exportFileDialog.DefaultExt = "txt";
            this.exportFileDialog.Filter = "txt (*.txt)|*.txt|jpg (*.jpg)|*.jpg|png (*.png)|*.png";
            this.exportFileDialog.InitialDirectory = "C:\\";
            this.exportFileDialog.RestoreDirectory = true;
            this.exportFileDialog.Title = "save outcome";
            // 
            // export
            // 
            this.export.Location = new System.Drawing.Point(452, 13);
            this.export.Name = "export";
            this.export.Size = new System.Drawing.Size(75, 25);
            this.export.TabIndex = 6;
            this.export.Text = "export";
            this.export.UseVisualStyleBackColor = true;
            this.export.Click += new System.EventHandler(this.export_Click);
            // 
            // import
            // 
            this.import.Location = new System.Drawing.Point(575, 13);
            this.import.Name = "import";
            this.import.Size = new System.Drawing.Size(75, 25);
            this.import.TabIndex = 7;
            this.import.Text = "import";
            this.import.UseVisualStyleBackColor = true;
            this.import.Click += new System.EventHandler(this.import_Click);
            // 
            // inclusion
            // 
            this.inclusion.Controls.Add(this.sizeOfIlncusion);
            this.inclusion.Controls.Add(this.typeOfInclusion);
            this.inclusion.Controls.Add(this.label3);
            this.inclusion.Controls.Add(this.label2);
            this.inclusion.Controls.Add(this.inclusionRandomButton);
            this.inclusion.Controls.Add(this.numberOfinclusion);
            this.inclusion.Location = new System.Drawing.Point(28, 293);
            this.inclusion.Name = "inclusion";
            this.inclusion.Size = new System.Drawing.Size(200, 109);
            this.inclusion.TabIndex = 8;
            this.inclusion.TabStop = false;
            this.inclusion.Text = "inclusion";
            // 
            // sizeOfIlncusion
            // 
            this.sizeOfIlncusion.Location = new System.Drawing.Point(70, 47);
            this.sizeOfIlncusion.Name = "sizeOfIlncusion";
            this.sizeOfIlncusion.Size = new System.Drawing.Size(34, 22);
            this.sizeOfIlncusion.TabIndex = 5;
            // 
            // typeOfInclusion
            // 
            this.typeOfInclusion.FormattingEnabled = true;
            this.typeOfInclusion.Items.AddRange(new object[] {
            "square",
            "circle"});
            this.typeOfInclusion.Location = new System.Drawing.Point(70, 17);
            this.typeOfInclusion.Name = "typeOfInclusion";
            this.typeOfInclusion.Size = new System.Drawing.Size(121, 24);
            this.typeOfInclusion.TabIndex = 4;
            this.typeOfInclusion.SelectedIndexChanged += new System.EventHandler(this.typeOfInclusion_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "size";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "type";
            // 
            // inclusionRandomButton
            // 
            this.inclusionRandomButton.Location = new System.Drawing.Point(51, 78);
            this.inclusionRandomButton.Name = "inclusionRandomButton";
            this.inclusionRandomButton.Size = new System.Drawing.Size(143, 25);
            this.inclusionRandomButton.TabIndex = 1;
            this.inclusionRandomButton.Text = "random inclusion";
            this.inclusionRandomButton.UseVisualStyleBackColor = true;
            this.inclusionRandomButton.Click += new System.EventHandler(this.inclusionRandomButton_Click);
            // 
            // numberOfinclusion
            // 
            this.numberOfinclusion.FormattingEnabled = true;
            this.numberOfinclusion.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.numberOfinclusion.Location = new System.Drawing.Point(6, 79);
            this.numberOfinclusion.Name = "numberOfinclusion";
            this.numberOfinclusion.Size = new System.Drawing.Size(38, 24);
            this.numberOfinclusion.TabIndex = 0;
            this.numberOfinclusion.SelectedIndexChanged += new System.EventHandler(this.numberOfinclusion_SelectedIndexChanged);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "txt";
            this.saveFileDialog.Filter = "txt (*.txt)|*.txt|jpg (*.jpg)|*.jpg|png (*.png)|*.png";
            this.saveFileDialog.InitialDirectory = "C:\\";
            this.saveFileDialog.RestoreDirectory = true;
            this.saveFileDialog.Title = "save outcome";
            // 
            // grainBoundaries
            // 
            this.grainBoundaries.Controls.Add(this.selectedBorder);
            this.grainBoundaries.Controls.Add(this.rmAll);
            this.grainBoundaries.Controls.Add(this.label7);
            this.grainBoundaries.Controls.Add(this.rmMatterial);
            this.grainBoundaries.Controls.Add(this.boundSize);
            this.grainBoundaries.Controls.Add(this.label6);
            this.grainBoundaries.Controls.Add(this.allBound);
            this.grainBoundaries.Location = new System.Drawing.Point(234, 23);
            this.grainBoundaries.Name = "grainBoundaries";
            this.grainBoundaries.Size = new System.Drawing.Size(200, 177);
            this.grainBoundaries.TabIndex = 9;
            this.grainBoundaries.TabStop = false;
            this.grainBoundaries.Text = "grain boundaries";
            // 
            // selectedBorder
            // 
            this.selectedBorder.Location = new System.Drawing.Point(23, 86);
            this.selectedBorder.Name = "selectedBorder";
            this.selectedBorder.Size = new System.Drawing.Size(145, 25);
            this.selectedBorder.TabIndex = 6;
            this.selectedBorder.Text = "selected boundaries";
            this.selectedBorder.UseVisualStyleBackColor = true;
            this.selectedBorder.Click += new System.EventHandler(this.selectedBorder_Click);
            // 
            // rmAll
            // 
            this.rmAll.Location = new System.Drawing.Point(109, 134);
            this.rmAll.Name = "rmAll";
            this.rmAll.Size = new System.Drawing.Size(75, 25);
            this.rmAll.TabIndex = 5;
            this.rmAll.Text = "all";
            this.rmAll.UseVisualStyleBackColor = true;
            this.rmAll.Click += new System.EventHandler(this.rmAll_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(77, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "clear";
            // 
            // rmMatterial
            // 
            this.rmMatterial.Location = new System.Drawing.Point(13, 134);
            this.rmMatterial.Name = "rmMatterial";
            this.rmMatterial.Size = new System.Drawing.Size(78, 25);
            this.rmMatterial.TabIndex = 3;
            this.rmMatterial.Text = "material";
            this.rmMatterial.UseVisualStyleBackColor = true;
            this.rmMatterial.Click += new System.EventHandler(this.rmMatterial_Click);
            // 
            // boundSize
            // 
            this.boundSize.Location = new System.Drawing.Point(112, 24);
            this.boundSize.Name = "boundSize";
            this.boundSize.Size = new System.Drawing.Size(34, 22);
            this.boundSize.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "boundaris size";
            // 
            // allBound
            // 
            this.allBound.Location = new System.Drawing.Point(23, 52);
            this.allBound.Name = "allBound";
            this.allBound.Size = new System.Drawing.Size(145, 25);
            this.allBound.TabIndex = 0;
            this.allBound.Text = "all boundaries";
            this.allBound.UseVisualStyleBackColor = true;
            this.allBound.Click += new System.EventHandler(this.allBound_Click);
            // 
            // elementToChoose
            // 
            this.elementToChoose.FormattingEnabled = true;
            this.elementToChoose.ItemHeight = 16;
            this.elementToChoose.Location = new System.Drawing.Point(247, 312);
            this.elementToChoose.Name = "elementToChoose";
            this.elementToChoose.Size = new System.Drawing.Size(80, 84);
            this.elementToChoose.TabIndex = 10;
            // 
            // ChoosenElement
            // 
            this.ChoosenElement.FormattingEnabled = true;
            this.ChoosenElement.ItemHeight = 16;
            this.ChoosenElement.Location = new System.Drawing.Point(343, 312);
            this.ChoosenElement.Name = "ChoosenElement";
            this.ChoosenElement.Size = new System.Drawing.Size(80, 84);
            this.ChoosenElement.TabIndex = 11;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(252, 402);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 25);
            this.addButton.TabIndex = 12;
            this.addButton.Text = "add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(343, 402);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 25);
            this.removeButton.TabIndex = 13;
            this.removeButton.Text = "remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Sub);
            this.groupBox1.Controls.Add(this.Dp);
            this.groupBox1.Location = new System.Drawing.Point(234, 206);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "substructures";
            // 
            // Sub
            // 
            this.Sub.Location = new System.Drawing.Point(43, 52);
            this.Sub.Name = "Sub";
            this.Sub.Size = new System.Drawing.Size(115, 25);
            this.Sub.TabIndex = 1;
            this.Sub.Text = "substructure";
            this.Sub.UseVisualStyleBackColor = true;
            this.Sub.Click += new System.EventHandler(this.Sub_Click);
            // 
            // Dp
            // 
            this.Dp.Location = new System.Drawing.Point(43, 21);
            this.Dp.Name = "Dp";
            this.Dp.Size = new System.Drawing.Size(115, 25);
            this.Dp.TabIndex = 0;
            this.Dp.Text = "Dual-Phase";
            this.Dp.UseVisualStyleBackColor = true;
            this.Dp.Click += new System.EventHandler(this.Dp_Click);
            // 
            // percentageOfBoundaries
            // 
            this.percentageOfBoundaries.AutoSize = true;
            this.percentageOfBoundaries.Location = new System.Drawing.Point(46, 410);
            this.percentageOfBoundaries.Name = "percentageOfBoundaries";
            this.percentageOfBoundaries.Size = new System.Drawing.Size(16, 17);
            this.percentageOfBoundaries.TabIndex = 15;
            this.percentageOfBoundaries.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(92, 410);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(20, 17);
            this.label9.TabIndex = 16;
            this.label9.Text = "%";
            // 
            // percentageOfBoundariesButton
            // 
            this.percentageOfBoundariesButton.Location = new System.Drawing.Point(129, 406);
            this.percentageOfBoundariesButton.Name = "percentageOfBoundariesButton";
            this.percentageOfBoundariesButton.Size = new System.Drawing.Size(75, 25);
            this.percentageOfBoundariesButton.TabIndex = 17;
            this.percentageOfBoundariesButton.Text = "calculate";
            this.percentageOfBoundariesButton.UseVisualStyleBackColor = true;
            this.percentageOfBoundariesButton.Click += new System.EventHandler(this.percentageOfBoundariesButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 460);
            this.Controls.Add(this.percentageOfBoundariesButton);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.percentageOfBoundaries);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.ChoosenElement);
            this.Controls.Add(this.elementToChoose);
            this.Controls.Add(this.grainBoundaries);
            this.Controls.Add(this.inclusion);
            this.Controls.Add(this.import);
            this.Controls.Add(this.export);
            this.Controls.Add(this.groupBoxGraph);
            this.Controls.Add(this.groupBoxSimpleGrainGrowth);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "GrainGrowthApplication";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBoxSimpleGrainGrowth.ResumeLayout(false);
            this.groupBoxSimpleGrainGrowth.PerformLayout();
            this.groupBoxGraph.ResumeLayout(false);
            this.groupBoxGraph.PerformLayout();
            this.inclusion.ResumeLayout(false);
            this.inclusion.PerformLayout();
            this.grainBoundaries.ResumeLayout(false);
            this.grainBoundaries.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button randomSeeds;
        private System.Windows.Forms.ComboBox numberOfSeeds;
        private System.Windows.Forms.Button simpleGrowthButton;
        private System.Windows.Forms.GroupBox groupBoxSimpleGrainGrowth;
        private System.Windows.Forms.GroupBox groupBoxGraph;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LabelWidth;
        private System.Windows.Forms.TextBox LabelHight;
        private System.Windows.Forms.Button setGraph;
        private System.Windows.Forms.OpenFileDialog importFileDialog;
        private System.Windows.Forms.SaveFileDialog exportFileDialog;
        private System.Windows.Forms.Button export;
        private System.Windows.Forms.Button import;
        private System.Windows.Forms.GroupBox inclusion;
        private System.Windows.Forms.Button inclusionRandomButton;
        private System.Windows.Forms.ComboBox numberOfinclusion;
        private System.Windows.Forms.TextBox sizeOfIlncusion;
        private System.Windows.Forms.ComboBox typeOfInclusion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button grianCurvatureButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textPropability;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.GroupBox grainBoundaries;
        private System.Windows.Forms.Button allBound;
        private System.Windows.Forms.TextBox boundSize;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button rmAll;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button rmMatterial;
        private System.Windows.Forms.ListBox elementToChoose;
        private System.Windows.Forms.ListBox ChoosenElement;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Sub;
        private System.Windows.Forms.Button Dp;
        private System.Windows.Forms.Button selectedBorder;
        private System.Windows.Forms.Label percentageOfBoundaries;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button percentageOfBoundariesButton;
    }
}

